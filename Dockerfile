FROM ubuntu:16.04
USER root

RUN apt-get update
RUN apt-get -y install software-properties-common net-tools wget curl nano \
    gcc make automake git dpkg-dev build-essential iputils-ping 


RUN export DEBIAN_FRONTEND=noninteractive && apt-get -y install apache2 mysql-server \
    php libapache2-mod-php php-mcrypt php-mysql \
    php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip

COPY ./src/default.conf /etc/apache2/sites-available/
COPY ./wordpress /var/www/wordpress
COPY ./wordpress /var/www/site

#RUN cd /tmp
RUN cd /tmp && curl -O https://wordpress.org/latest.tar.gz
RUN cd /tmp && tar xzvf latest.tar.gz
RUN touch /tmp/wordpress/.htaccess
RUN cp /tmp/wordpress/wp-config-sample.php /tmp/wordpress/wp-config.php
RUN mkdir /tmp/wordpress/wp-content/upgrade
RUN cp -a /tmp/wordpress/. /var/www/wordpress
RUN cd /tmp && curl -s https://api.wordpress.org/secret-key/1.1/salt/

#RUN apt-get -y install nginx


COPY ./src/wordpress.sh /etc/
RUN /etc/wordpress.sh


CMD service apache2 && sleep infinity
