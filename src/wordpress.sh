#!/bin/bash


######## DOMAIN NAME ########
domain='realestate.example.com'


######## LAMP and ROUNDCUBE ########
rm /etc/apache2/sites-available/000-default.conf
rm /etc/apache2/sites-available/default-ssl.conf
rm /etc/apache2/sites-enabled/000-default.conf

sed -i "1s/.*/ServerName $domain/" /etc/apache2/apache2.conf

service apache2 start
a2ensite default
a2enmod rewrite
apache2ctl restart

chown -R www-data:www-data /var/www/wordpress
find /var/www/wordpress/ -type d -exec chmod 750 {} \;
find /var/www/wordpress/ -type f -exec chmod 640 {} \;

######## MYSQL ########
service mysql start
mysql -u root -e "SET PASSWORD FOR root@localhost = PASSWORD('password')";
mysql -u root -ppassword <<MYSQL_SCRIPT
CREATE DATABASE wordpress DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
CREATE USER 'wordpressuser'@'localhost' IDENTIFIED BY '';
GRANT ALL PRIVILEGES ON wordpress.* to 'wordpressuser'@'localhost';
FLUSH PRIVILEGES;
EXIT
MYSQL_SCRIPT

sed -i "23s/.*/define( 'DB_NAME', 'wordpress' );/" /var/www/wordpress/wp-config.php
sed -i "26s/.*/define( 'DB_USER', 'wordpressuser' );/" /var/www/wordpress/wp-config.php
sed -i "29s/.*/define( 'DB_PASSWORD', '' );/" /var/www/wordpress/wp-config.php
